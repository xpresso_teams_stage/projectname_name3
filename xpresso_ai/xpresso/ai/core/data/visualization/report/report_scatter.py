import os
import shutil
import uuid
import tqdm
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.automl import utility
from xpresso.ai.core.commons.utils.constants import REPORT_OUTPUT_PATH, \
    EMPTY_STRING
from xpresso.ai.core.data.visualization.report import Report


class ScatterReport(Report):
    def __init__(self, dataset):
        super().__init__(dataset)

    def create_report(self, input_path=utils.DEFAULT_IMAGE_PATH,
                      output_path=REPORT_OUTPUT_PATH, file_name=None):
        """
        Function to create the report for the metrics of all the attributes
        Args:
            input_path (str): path to the plots to be fetched into the report
            output_path (str): path to the folder the report to be saved
            file_name(str): file name of report file
        """
        if file_name is None or file_name == EMPTY_STRING:
            file_name = "report_scatter_{}.pdf".format(str(uuid.uuid4()))
        output_path, output_file_name = utility.get_file_from_path_string(
            output_path,
            file_name)
        if output_path is None:
            output_path = REPORT_OUTPUT_PATH
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        output_file_name = utility.set_extension(output_file_name, ".pdf")
        self.title_page()
        attribute_file_mapping = self.attribute_file_mapping()
        for attr in tqdm.tqdm(self.dataset.info.attributeInfo):
            try:
                scatter_files = attribute_file_mapping[attr.name]["scatter"]
            except KeyError:
                continue
            self.add_scatter_plots(file_names=scatter_files)
        self.output(os.path.join(output_path, output_file_name), 'F')
        if os.path.exists(input_path):
            shutil.rmtree(input_path)
